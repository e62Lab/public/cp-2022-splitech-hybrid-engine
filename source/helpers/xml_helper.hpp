#ifndef XML_HELPER_HPP
#define XML_HELPER_HPP

#include <medusa/Medusa_fwd.hpp>

/**
 * @brief Get the config files object.
 *
 * @param conf Master configuration.
 * @return mm::Range<mm::XML> Range of configurations with a single radius.
 */
mm::Range<mm::XML> get_config_files(const mm::XML& conf) {
    // Initialize list of output XMLS.
    mm::Range<std::string> rbffd_radiuses = mm::split(conf.get<std::string>("nodes.r"), ',');
    mm::Range<std::string> mon_degree_list =
        mm::split(conf.get<std::string>("approx.mon_degree"), ',');
    mm::Range<mm::XML> all_conf;

    for (string rbffd_radius : rbffd_radiuses) {
        for (string mon_degree : mon_degree_list) {
            // Copy master configuration.
            mm::XML _conf(conf);

            // Set radius.
            _conf.set("nodes.r", rbffd_radius, true);
            _conf.set("approx.mon_degree", mon_degree, true);

            // Set output filenames.
            std::string out_file = _conf.get<std::string>("meta.out_file");
            _conf.set("meta.out_file", mm::format("%s_r%s_m%s", out_file, rbffd_radius, mon_degree),
                      true);

            // Append to range.
            all_conf.push_back(_conf);
        }
    }

    if (conf.get<int>("debug.print") == 1) {
        cout << "XML range created." << endl;
    }

    return all_conf;
}

#endif  // XML_HELPER_HPP