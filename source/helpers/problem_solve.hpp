#ifndef PROBLEM_SOLVE_HPP
#define PROBLEM_SOLVE_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include "shapes.hpp"

template <typename vec_t>
Eigen::VectorXd solve_problem(mm::DomainDiscretization<vec_t>& domain, const mm::XML& conf,
                              mm::Timer& timer, mm::HDF& hdf, std::string& basis_type,
                              int run_idx) {
    auto N = domain.size();
    mm::Range<int> rbffd_nodes, wls_nodes;

    if (basis_type == "phs") {
        rbffd_nodes = domain.all();
    } else if (basis_type == "mon") {
        wls_nodes = domain.all();
    } else if (basis_type == "hybrid") {
        auto r = conf.get<double>("nodes.r");
        vec_t source_origin;
        source_origin.setConstant(0.5);
        for (int i : domain.all()) {
            vec_t p = domain.pos(i);

            double len = (p - source_origin).norm();

            if (len <= r) {
                rbffd_nodes.push_back(i);
            } else {
                wls_nodes.push_back(i);
            };
        }
        if (run_idx == 0) {
            // Write nodes to file.
            hdf.atomic().writeIntArray("nodes_rbf", rbffd_nodes);
            hdf.atomic().writeIntArray("nodes_wls", wls_nodes);
        }
    } else {
        assert_msg(false, "Unknown basis type '%s'.", basis_type);
    }

    // Compute shapes.
    using MatrixType = Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
    auto storage = compute_shapes<vec_t, MatrixType>(domain, conf, timer, basis_type, rbffd_nodes,
                                                     wls_nodes, run_idx);

    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);
    auto op = storage.implicitOperators(M, rhs);
    for (int i : domain.interior()) {
        op.lap(i) = u_laplacian(domain.pos(i));
    }
    for (int i : domain.boundary()) {
        op.value(i) = u_analytic(domain.pos(i));
    }
    // timer.addCheckPoint(mm::format("compute_%s_run_%03d", basis_type, run_idx));
    Eigen::SparseLU<decltype(M)> solver;
    solver.compute(M);
    // timer.addCheckPoint(mm::format("solve_%s_run_%03d", basis_type, run_idx));
    Eigen::VectorXd sol = solver.solve(rhs);

    // timer.addCheckPoint(mm::format("postprocess_%s_run_%03d", basis_type, run_idx));

    return sol;
}

#endif  // PROBLEM_SOLVE_HPP