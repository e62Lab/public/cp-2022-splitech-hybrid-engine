#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <medusa/Medusa.hpp>
#include "math_helper.hpp"

template <typename vec_t, typename matrix_t>
mm::RaggedShapeStorage<
    vec_t, std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>, mm::Der2s<vec_t::dim>>>
compute_shapes(mm::DomainDiscretization<vec_t>& domain, const mm::XML& conf, mm::Timer& timer,
               std::string& basis_type, mm::Range<int>& rbffd_nodes, mm::Range<int>& wls_nodes,
               int run_idx) {
    auto N = domain.size();

    // Originally there was only Lap shape. But since the point contact as benchmark, other
    // derivatives were added. This may change the absolute computational times, but not the
    // relative.
    mm::RaggedShapeStorage<
        vec_t, std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>, mm::Der2s<vec_t::dim>>>
        storage;

    // Engines.
    auto k = conf.get<int>("approx.k");
    auto aug = conf.get<int>("approx.mon_degree");
    // mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest, mm::JacobiSVDWrapper<typename
    // vec_t::scalar_t, Eigen::ColPivHouseholderQRPreconditioner>>
    //     rbffd_engine(k, aug);
    mm::RBFFD<mm::Polyharmonic<double>, vec_t, mm::ScaleToClosest, Eigen::PartialPivLU<matrix_t>>
        rbffd_engine(k, aug);

    double sigmaW = conf.get<double>("approx.sigma_w");
    // mm::WLS<mm::Monomials<vec_t>, mm::GaussianWeight<vec_t>, mm::ScaleToClosest> wls_engine(
    //     mm::Monomials<vec_t>(aug), sigmaW);
    mm::WLS<mm::Monomials<vec_t>, mm::GaussianWeight<vec_t>, mm::ScaleToClosest,
            Eigen::ColPivHouseholderQR<matrix_t>>
        wls_engine(mm::Monomials<vec_t>(aug), sigmaW);
    // mm::WLS<mm::Monomials<vec_t>, mm::NoWeight<vec_t>, mm::ScaleToFarthest,
    // Eigen::PartialPivLU<matrix_t>> wls_engine(mm::Monomials<vec_t>(2));
    // mm::WLS<mm::Monomials<vec_t>> wls_engine(2);
    // Get support size.
    int n = conf.get<int>("approx.support_size");
    if (n == -1) {
        n = 2 * binomialCoeff(conf.get<int>("approx.mon_degree") + vec_t::dim, vec_t::dim);
    }

    // Compute supports.
    domain.findSupport(mm::FindClosest(n));

    std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>, mm::Der2s<vec_t::dim>> operators;
    mm::Range<int> uu(domain.size(), n);
    storage.resize(uu);

    // Compute shapes.
    // timer.addCheckPoint(mm::format("shapes_%s_run_%03d", basis_type, run_idx));
    mm::computeShapes(domain, rbffd_engine, rbffd_nodes, operators, &storage);
    mm::computeShapes(domain, wls_engine, wls_nodes, operators, &storage);
    // timer.addCheckPoint(mm::format("matrix_%s_run_%03d", basis_type, run_idx));

    return storage;
}

#endif  // SHAPES_HPP