#ifndef STEEP_SOURCE_HELPER_HPP
#define STEEP_SOURCE_HELPER_HPP

#include <medusa/Medusa_fwd.hpp>
#include <math.h>

const double alpha = 1000.0;
const double source_location = 0.5;

template <typename vec_t>
typename vec_t::scalar_t u_analytic(const vec_t& p) {
    vec_t source = vec_t::Constant(source_location);
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return exp(exponent);
}

template <typename vec_t>
typename vec_t::scalar_t u_laplacian(const vec_t& p) {
    vec_t source = vec_t::Constant(source_location);
    double squared_norm = (p - source).squaredNorm();
    double exponent = -alpha * squared_norm;

    return 4.0 * exp(exponent) * (mm::ipow<2>(alpha) * squared_norm - alpha);
}

template <typename vec_t>
typename vec_t::scalar_t u_analytic_slak(const vec_t& p) {
    return 1.0 / (25 * (4 * p - vec_t::Constant(2)).squaredNorm() + 1);
}

template <typename vec_t>
typename vec_t::scalar_t u_laplacian_slak(const vec_t& p) {
    auto r2 = 25 * (4 * p - vec_t::Constant(2)).squaredNorm();
    return 3200 * r2 / mm::ipow<3>(1 + r2) - 800 * vec_t::dim / mm::ipow<2>(1 + r2);
}

#endif  // STEEP_SOURCE_HELPER_HPP