#include <medusa/Medusa.hpp>
#include <medusa/bits/domains/GeneralFill.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <iostream>
#include <Eigen/Sparse>

#include "helpers/math_helper.hpp"
#include "helpers/shapes.hpp"
#include "helpers/xml_helper_contact.hpp"

/// Classical 3D Point contact problem.
/// http://e6.ijs.si/medusa/wiki/index.php/Linear_elasticity#Point_contact_3D

using namespace mm;  // NOLINT

int main(int argc, const char* argv[]) {
    // Read input.
    assert_msg(argc >= 2, "Second argument should be the parameter xml.");
    XML master_conf(argv[1]);
    // omp_set_num_threads(master_conf.get<int>("sys.threads"));

    auto configs = get_config_files(master_conf);
#pragma omp parallel for
    for (auto conf_id = 0; conf_id < configs.size(); conf_id++) {
        auto conf = configs[conf_id];

        Timer timer;
        timer.addCheckPoint("start");

        // Physical parameters
        const double E = conf.get<double>("phy.E");
        const double nu = conf.get<double>("phy.nu");
        const double P = -conf.get<double>("phy.P");

        // Derived parameters
        double lam = E * nu / (1 - 2 * nu) / (1 + nu);
        const double mu = E / 2 / (1 + nu);

        // Closed form solution
        std::function<Vec3d(Vec3d)> analytical = [=](const Vec3d& p) {
            double x = p[0], y = p[1], z = p[2];
            double r = std::sqrt(x * x + y * y);
            double c = x / r, s = y / r;
            double R = p.norm();
            double u = P * r / 4 / PI / mu * (z / R / R / R - (1 - 2 * nu) / (R * (R + z)));
            double w = P / 4 / PI / mu * (z * z / R / R / R + 2 * (1 - nu) / R);
            return Vec3d(u * c, u * s, w);
        };

        // Domain definition
        double a = conf.get<double>("num.a");
        double b = conf.get<double>("num.b");
        Vec3d low = -a, high = -b;
        BoxShape<Vec3d> box(low, high);

        double step = conf.get<double>("num.h");

        std::function<double(Vec3d)> density = [=](const Vec3d& v) {
            double r = (v - low).head<2>().norm() / (high - low).head<2>().norm();
            return step * (9 * (1 - r) + 1);
        };
        GeneralFill<Vec3d> fill;
        fill.seed(0);
        DomainDiscretization<Vec3d> domain = box.discretizeWithDensity(density, fill);
        int N = domain.size();
        prn(N);

        vector<string> basis_types = split(conf.get<string>("approx.basis_type"), ',');
        // basis_types.push_back("hybrid");

        // Write the solution into a file.
        stringstream out_hdf;
        out_hdf << conf.get<string>("meta.out_dir") << conf.get<string>("meta.out_file") << ".h5";
        HDF hdf(out_hdf.str(), HDF::DESTROY);
        hdf.writeXML("conf", conf);
        hdf.close();
        hdf.atomic().writeDoubleAttribute("E", E);
        hdf.atomic().writeDoubleAttribute("nu", nu);
        hdf.atomic().writeDoubleAttribute("P", P);
        hdf.atomic().writeDomain("domain", domain);

        for (auto basis_type : basis_types) {
            mm::Range<int> rbffd_nodes, wls_nodes;
            prn(basis_type) if (basis_type == "phs") { rbffd_nodes = domain.all(); }
            else if (basis_type == "mon") {
                wls_nodes = domain.all();
            }
            else if (basis_type == "hybrid") {
                auto r = conf.get<double>("nodes.r");
                Vec3d source_origin;
                source_origin.setConstant(-b);
                for (int i : domain.all()) {
                    Vec3d p = domain.pos(i);

                    double len = (p - source_origin).norm();

                    if (len <= r) {
                        rbffd_nodes.push_back(i);
                    } else {
                        wls_nodes.push_back(i);
                    };
                }
                // Write nodes to file.
                hdf.atomic().writeIntArray("nodes_rbf", rbffd_nodes);
                hdf.atomic().writeIntArray("nodes_wls", wls_nodes);
            }
            else {
                assert_msg(false, "Unknown basis type '%s'.", basis_type);
            }

            // Compute shapes.
            using MatrixType =
                Eigen::Matrix<typename Vec3d::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
            auto storage = compute_shapes<Vec3d, MatrixType>(domain, conf, timer, basis_type,
                                                             rbffd_nodes, wls_nodes, 0);

            std::cerr << "Shapes computed, solving system." << std::endl;

            Eigen::SparseMatrix<double, Eigen::RowMajor> M(3 * N, 3 * N);
            Eigen::VectorXd rhs(3 * N);
            rhs.setZero();
            M.reserve(storage.supportSizesVec());

            // Construct implicit operators over our storage.
            auto op = storage.implicitVectorOperators(M, rhs);
            // Set the governing equations and the boundary conditions.
            for (int i : domain.interior()) {
                (lam + mu) * op.graddiv(i) + mu* op.lap(i) = 0.0;
            }
            for (int i : domain.boundary()) {
                op.value(i) = analytical(domain.pos(i));
            }

            Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
            solver.preconditioner().setFillfactor(30);
            solver.preconditioner().setDroptol(1e-5);
            solver.setMaxIterations(500);
            solver.setTolerance(1e-14);
            // Eigen::SparseLU<decltype(M)> solver;
            // solver.compute(M);
            // auto sol = solver.solve(rhs);
            solver.compute(M);
            Eigen::VectorXd solution = solver.solve(rhs);

            prn(solver.iterations());
            prn(solver.error());
            VectorField3d u = VectorField3d::fromLinear(solution);
            hdf.atomic().writeEigen(format("displacement_%s", basis_type), u);
        }
        timer.addCheckPoint("end");

        hdf.atomic().writeTimer("time", timer);
    }
    return 0;
}
