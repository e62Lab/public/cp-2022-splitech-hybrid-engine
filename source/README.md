# Source files

- The `contact.cpp` is used for a 3D point contact problem.
- The `main.cpp` is used for a synthetic example with a two-dimensional Poisson problem with an exponentially strong source.
