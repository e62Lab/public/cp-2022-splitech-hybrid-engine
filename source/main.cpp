#include <iostream>
#include <medusa/Medusa_fwd.hpp>
#include "helpers/steep_source_helper.hpp"
#include "helpers/problem_solve.hpp"
#include "helpers/xml_helper.hpp"

using namespace std;
using namespace mm;
using namespace Eigen;

/**
 * @brief Running case.
 *
 * @tparam vec_t Vector.
 * @param conf Configuration hdf.
 * @param hdf Output HDF hdf
 */
template <typename vec_t>
void run_cases(const XML& conf, HDF& hdf) {
    vector<string> nodes_per_side_list =
        split(conf.get<string>("discretization.nodes_per_side"), ',');
    vector<string> basis_types = split(conf.get<string>("approx.basis_type"), ',');

    // Run for all discretizations.
    // #pragma omp parallel for
    for (int i = 0; i < nodes_per_side_list.size(); ++i) {
        double nodes_per_side = stod(nodes_per_side_list[i]);
        if (conf.get<int>("debug.print") == 1) {
            cout << "---------------------" << endl;
            cout << "Computation for nodes_per_side = " << nodes_per_side << " started." << endl;
        }
        double dx = 1.0 / nodes_per_side;
        double small_dx = dx / conf.get<double>("discretization.small_dx_factor");
        vec_t center = 0.5;

        // Initialize HDF group.
        hdf.setGroupName(format("/iter_%06d/", i));
        // Add attributes to hdf.
        hdf.atomic().writeDoubleAttribute("dx", dx);
        hdf.atomic().writeDoubleAttribute("small_dx", small_dx);
        hdf.atomic().writeIntAttribute("nodes_per_side", nodes_per_side);

        // Create density function.
        auto h = [=](const vec_t& p) {
            return min(small_dx + (dx - small_dx) * pow((p - center).norm(), 1.5), dx);
        };

        int N_runs = conf.get<int>("general.runs");
        Timer t;
        t.addCheckPoint("start");
#pragma omp parallel for
        for (int i_run = 0; i_run < N_runs; i_run++) {
            // Discretize domain.
            if (conf.get<int>("debug.print") == 1) {
                cout << "Discretizing domain." << endl;
            }
            BallShape<vec_t> shape(conf.get<double>("domain.origin"),
                                   conf.get<int>("domain.radius"));
            auto domain = shape.discretizeBoundaryWithDensity(h);
            GeneralFill<vec_t> fill;
            fill.seed(get_seed());
            fill(domain, h);
            int N = domain.size();
            if (conf.get<int>("debug.print") == 1) {
                prn(N)
            }
            hdf.atomic().writeIntAttribute(format("N_run_%02d", i_run), N);
            if (i_run == 0) {
                // Add domain to hdf.
                hdf.atomic().writeDomain("domain", domain);
            }
            // Compute analytic solution.
            Eigen::VectorXd sol_analytic(N);
            for (int k = 0; k < N; ++k) {
                sol_analytic[k] = u_analytic(domain.pos(k));
            }
            double sol_analytic_norm_1 = sol_analytic.lpNorm<1>();
            double sol_analytic_norm_2 = sol_analytic.lpNorm<2>();
            double sol_analytic_norm_inf = sol_analytic.lpNorm<Eigen::Infinity>();

            // Run for all basis types.
            for (int basis_type_idx = 0; basis_type_idx < basis_types.size(); basis_type_idx++) {
                string basis_type = basis_types[basis_type_idx];
                if (conf.get<int>("debug.print") == 1) {
                    prn(basis_type);
                }

                VectorXd sol = solve_problem<vec_t>(domain, conf, t, hdf, basis_type, i_run);
                // Compute error.
                if (i_run == 0) {
                    // Add to hdf.
                    hdf.atomic().writeDoubleArray(format("solution_%s", basis_type), sol);
                }
                VectorXd err = sol_analytic - sol;
                // Compute error norm.
                double err_norm_1 = err.lpNorm<1>() / sol_analytic_norm_1;
                double err_norm_2 = err.lpNorm<2>() / sol_analytic_norm_2;
                double err_norm_inf = err.lpNorm<Eigen::Infinity>() / sol_analytic_norm_inf;
                // Add error norms to hdf.
                hdf.atomic().writeDoubleAttribute(
                    format("err_norm_1_%s_run_%02d", basis_type, i_run), err_norm_1);
                hdf.atomic().writeDoubleAttribute(
                    format("err_norm_2_%s_run_%02d", basis_type, i_run), err_norm_2);
                hdf.atomic().writeDoubleAttribute(
                    format("err_norm_inf_%s_run_%02d", basis_type, i_run), err_norm_inf);
            }
        }

        // Add timer to hdf.
        t.addCheckPoint("end");
        // hdf.atomic().writeTimer("time", t);
    }
};

int main(int argc, const char* argv[]) {
    // Read input.
    assert_msg(argc >= 2, "Second argument should be the parameter xml.");
    XML conf(argv[1]);

    // Set num threads.
    omp_set_num_threads(conf.get<int>("sys.num_threads"));

    // Run for multiple rbffd radiuses.
    // Generate configs.
    auto configs = get_config_files(conf);
    prn(configs.size());
    // #pragma omp parallel for
    for (int i = 0; i < configs.size(); ++i) {
        auto config = configs[i];
        prn(config);
        // Create output.
        stringstream out_hdf;
        out_hdf << config.get<string>("meta.out_dir") << config.get<string>("meta.out_file")
                << ".h5";
        HDF hdf(out_hdf.str(), HDF::DESTROY);
        hdf.writeXML("conf", config);
        hdf.close();

        // Run.
        int dim = config.get<int>("domain.dim");
        switch (dim) {
            case 2:
                run_cases<Vec2d>(config, hdf);
                break;
            case 3:
                cout << "------------" << endl;
                cout << "WARNING: Dimensionality 3 not tested. Proceed with caution." << endl;
                cout << "------------" << endl;
                run_cases<Vec3d>(config, hdf);
                break;
            default:
                assert_msg(false, "Dimension %d not supported.", dim);
        }
    }

    return 0;
}
