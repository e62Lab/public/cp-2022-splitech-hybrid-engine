# Conference paper

Conference paper using template from [here](https://www.ieee.org/conferences/publishing/templates.html).

All figures stored in `../results/` directory.
