# Hybrid WLS and RBF-FD engine

Conference paper for Splitech 2022 on hybrid WLS and RBF-FD engine.

## Abstract

Since the advent of meshless methods as a tool for the numerical analysis of systems of Partial Differential Equations (PDEs), many variants have been proposed. In this work, we present a novel local meshless strong-form method that combines the stability of Radial Basis Function-Generated Finite Differences (RBF-FD) with the computational stability of Diffuse Approximation Method (DAM). To demonstrate the advantages of a novel hybrid method, we evaluate its numerical performance and computational complexity by solving a two-dimensional Poisson problem with a strong exponential source within the computational domain.

## Description

[Medusa](https://e6.ijs.si/medusa/) supports different approximation engines. Most comonly used are:

- the **radial basis function-generated finite differences** using the Polyharmonic splines augmented with monomials and
- the **weighted least squares** (WLS) approach using only monomials, also known as Diffuse Approximate Method.

### Goal

While the RBF-FD is more stable it is also computationally more expensive. In this paper the aim is to present a hybrid method combining the stability of RBF-FD and computational simplicity of WLS by using the RBF-FD where stability of the numerical solution might be an issue (close to some strong source for example) and WLS elsewhere.

Convergence rates and computational times are studied.

## Visuals

For progress and certain visuals please check the logs.

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.8.10 (or higher)
- Jupyter notebooks

## Usage

Create or go to `build/` directoy and build using

```bash
cmake .. && make -j 12
```

The executable will be created in `bin/` directory. The executable must be run with a parameter with all the settings, e.g.

```bash
./hybrid ../input/settings.xml
```

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

- **Mitja Jančič** under supervision of
- **Gregor Kosec**

## Acknowledgments

The authors would like to acknowledge the financial support of the ARRS research core funding No. P2-0095, ARRS project funding No. J2-3048 and the World Federation of Scientists.

# Reproducing paper results

Build and navigate to `bin/` directory, then:

- To reproduce the results from a synthetic example, run
  ```bash
  ./hybrid ../input/settings.xml
  ```
- To reproduce the point contact results, run
  ```bash
  ./contact ../input/contact.xml
  ```

# License

The project is open source and free to use.
