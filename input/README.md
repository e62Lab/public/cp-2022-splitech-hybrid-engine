# Input files

Usually out code has input configuration files written in `.json` or `.xml`. Such configuration files should be stored here.

## Details

Specific settings:

- `nodes.r`: Defines a circle around source. Nodes within are assigned to RBF-FD, the rest are WLS.
- `case.name`: Different cases are supported by the source code. For this project, `steep_source` is expected.
- `basis_type`: "phs" or "mon" or "hybrid.
- When `support_size = -1`, the recommended support size $' n = 2 \binom{m + d}{d} '$ is computed otherwise the value from XML is taken.
- `solver_type`: "pardiso" or "sparselu".
  > For `pardiso` you will have to uncomment some lines but generally speaking it should work.
