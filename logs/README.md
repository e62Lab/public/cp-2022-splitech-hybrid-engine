# Log

## 25. 01. 2022

- Repo created.
- Source added.
- Successful build.
- Successful run.
- Added RBF-FD and WLS nodes to hdf.

## 26. 01. 2022

- Added plot script.
- Numerical solution looks OK.

  <img src="results/example_solution.png" width="300"/>

- RBF-FD and WLS node division looks good.

  <img src="results/approximation_engine.png" width="300"/>

- Convergence rates looking good.

  <img src="results/convergence_rates.png" width="300"/>

- Computational times are wrong.

## 01. 02. 2022

- Computational times are difficult. To solve the small system, the RBF-FD by default uses solver `Eigen::PartialPivLU<matrix_t>` which turns out to be considerably faster than the default WLS solver `JacobiSVD`. However, the RBF-FD solver can be changed, for example to `mm::JacobiSVDWrapper<typename vec_t::scalar_t, Eigen::ColPivHouseholderQRPreconditioner>`, but this no longer makes sense. By changing the solver, times are better.
  <img src="results/times.png" width="300"/>
- LU is used for quadratic systems, which are in case of RBF-FD approximation but not in case of WLS approximation. By default JacobbiSVD is used in WLS and LU in RBF-FD to solve the small systems. Now using the SVD is expensive and can thus lead to RBF-FD being faster than the WLS solely due to the solver type used to solve the small systems.
- So instead of using the SVD, we use the HousholderQR (`Eigen::ColPivHouseholderQR<matrix_t>`) for the WLS.
- The complexity of LU is $`2/3 n^3`$ and complexity of HouseholderQr is $`2mn^2 - 2n^3/3`$ which is comparable. And Jacobi:
  > This JacobiSVD class is a two-sided Jacobi R-SVD decomposition, ensuring optimal reliability and accuracy. The downside is that it's slower than bidiagonalizing SVD algorithms for large square matrices; however its complexity is still $`O(n^2p)`$ where n is the smaller dimension and p is the greater dimension, meaning that it is still of the same order of complexity as the faster bidiagonalizing R-SVD algorithms.

## 16. 02. 2022

- Added paper template.
- Issue. Ok, the issue was with the convergence rates. The shape computation times are expected. The RBF-FD is the most expensive and WLS is the cheapest while the Hybrid is in between. The issue is that the convergence rates were very similar to WLS, not exactly the same but clearly more similar to WLS that RBF-FD or anything in-between. If that is the case, than a hybrid method makes absolutely no sense or better said: if there is no advantage to the hybrid method, then why use it? I mean sure, the computational times are reduced compared to the rbf-fd but the convergence rate for second degree monomials is equally as bad as pure WLS.

  So the idea is to check higher order approximations. And check if maybe the stability (spread over median error) is reduced by the hybrid method. In that case, hybrid could make sense for higher order approximations which would justify it's usage. Code has been adopted accordingly.

## 25. 02. 2022

- Updated source in the domain
- Parallel execution for convergence rates.

## 01. 03. 2022

- After scanning point contact results, several cases where WLS failed but hybrid worked were found:

  - contact_r0.5_m4_h0.008_k5_sigma1.5.h5
  - contact_r0.5_m4_h0.01_k3_sigma1.5.h5
  - contact_r0.5_m4_h0.01_k5_sigma1.5.h5
  - contact_r0.6_m4_h0.008_k3_sigma1.5.h5
  - contact_r0.6_m4_h0.01_k3_sigma1.5.h5
  - contact_r0.6_m4_h0.01_k5_sigma1.5.h5

  ![hybrid vs. wls solution of point contact](screenshots/contact_wls_hybrid.png)

## 02. 03. 2022

- First paper draft.
- Repository cleanup.
